package com.example.harjoitustehtava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    EditText searchByName;
    TextView detailsView;
    TextView matchCount;
    Button btnSearch;
    ListView companyListView;
    ArrayList<Company> companyList;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();

        companyList = new ArrayList<>();

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailsView.setText("");
                companyList.clear();
                matchCount.setText("");
                String name = searchByName.getText().toString();
                requestCompanyByName(name);
                populateCompanyListView();
            }
        });

        companyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getSelectedCompany(position);
            }
        });
    }

    private void initializeComponents() {
        searchByName = findViewById(R.id.searchbyName);
        detailsView = findViewById(R.id.detailsView);
        matchCount = findViewById(R.id.matchCount);
        companyListView = findViewById(R.id.companyListView);
        btnSearch = findViewById(R.id.btnSearch);
    }

    private void requestCompanyByName(String name){
        String url = "https://avoindata.prh.fi/bis/v1?&maxResults=100&name=" + name;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        parseResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                    }
                }
        );
        requestQueue.add(objectRequest);
    }

    private void parseResponse(JSONObject response) {
        try {
            JSONArray jsonArray = response.getJSONArray("results");
            companyList.clear();
            adapter.notifyDataSetChanged();

            parseResultsArray(jsonArray);
            companiesFoundCount(jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseResultsArray(JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                if (i < 10) // Show only 10 items in companyListView
                {
                    JSONObject result = jsonArray.getJSONObject(i);
                    Company company = new Company();
                    company.businessId = result.getString("businessId");
                    company.name = result.getString("name");

                    companyList.add(company);
                    adapter.notifyDataSetChanged();
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private void companiesFoundCount(JSONArray jsonArray) {
        matchCount.setText("Found: " + jsonArray.length() + " matches");
    }

    private void populateCompanyListView() {
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, companyList);
        companyListView.setAdapter(adapter);
    }

    private void getSelectedCompany(int position) {
        Company company = (Company) adapter.getItem(position);
        if (company != null)
            requestCompanyDetails(company);
    }

    private void requestCompanyDetails(final Company company){
        String url = "https://avoindata.prh.fi/bis/v1/" + company.businessId;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray results = (JSONArray) response.get("results");

                            for (int i = 0; i < results.length(); i++) {
                                JSONObject result = results.getJSONObject(i);

                                JSONArray addressesArray = (JSONArray) result.get("addresses");
                                Address address = parseAddressesArray(addressesArray);

                                JSONArray contactDetailsArray = (JSONArray) result.get("contactDetails");
                                ContactDetail contactDetail = parseContactDetailsArray(contactDetailsArray);

                                detailsView.setText(company.name + "\n" +
                                        company.businessId + "\n" +
                                        contactDetail.value + "\n" +
                                        address.street + "\n" +
                                        address.postCode + " " + address.city);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Rest Response", error.toString());
                    }
                }
        );
        requestQueue.add(objectRequest);
    }

    private Address parseAddressesArray(JSONArray addressesArray){
        Address address = new Address();
        try {
            for (int i = 0; i < addressesArray.length(); i++) {
                JSONObject addressJObject = addressesArray.getJSONObject(i);
                address.street = addressJObject.getString("street");
                address.postCode = addressJObject.getString("postCode");
                address.city = addressJObject.getString("city");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return address;
    }

    private ContactDetail parseContactDetailsArray(JSONArray contactDetailsArray) {
        ContactDetail contactDetail = new ContactDetail();
        try {
            for (int i = 0; i < contactDetailsArray.length(); i++) {
                JSONObject contact = contactDetailsArray.getJSONObject(i);

                String value = contact.getString("value");
                contactDetail.value = value;

                // Open browser if www-address is found
                String type = contact.getString("type");
                openBrowser(type, value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return contactDetail;
    }

    private void openBrowser(String type, String value) {
        if (type.equals("Kotisivun www-osoite") || type.equals("www-adress")
                || type.equals("Website address"))
        {
            if (value.length() > 0) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + value));
                startActivity(browserIntent);
            }
        }
    }

}


