package com.example.harjoitustehtava;

public class Company {

    String businessId;
    String name;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company() {
    }

    public Company(String businessId, String name) {
        this.businessId = businessId;
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " " + businessId;
    }
}
