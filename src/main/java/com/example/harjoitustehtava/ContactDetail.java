package com.example.harjoitustehtava;

public class ContactDetail {

    String value;
    String type;


    public ContactDetail() {
    }

    public ContactDetail(String value) {
        this.value = value;
    }

    public ContactDetail(String value, String type) {
        super();
        this.value = value;
        this.type = type;
    }

}
