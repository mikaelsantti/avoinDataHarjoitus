# AvoinDataHarjoitus

Harjoitustehtävä työpaikkaa varten.
Tämän harjoitustehtävän aiheena on luoda Android-sovellus.
Sovelluksen tulee olla natiivi (Java/Kotlin). 
Sovelluksessa käyttäjä voi hakea Suomessa YTJ-rekisterissä olevia yrityksiä.
Tietolähteenä käytetään PRH:n avointa dataa (https://avoindata.prh.fi/ytj.html).

Käyttäjä hakee yrityksiä nimellä tai sen alulla (GET /BIS/V1).
Sovellus listaa löydetyt tulokset. Listauksessa tulee näkyä jokaisen yrityksen nimi ja yritystunnus.
Näkymässä voi vain näkyä 10 ensimmäistä yritystä. Sovelluksen tulee näyttää kuinka monta osumaa kaikkiaan löytyi kyseisellä haulla.

Kun käyttäjä klikkaa valittua yritystä sovelluksen tulee hakea yrityksen tarkemmat tiedot (GET /BIS/V1/{BUSINESSID}).
Jos yrityksen tiedoista löytyy verkkosivu, sovellus avaa selaimen tähän osoitteeseen.
