package com.example.harjoitustehtava;

public class Address {

    String street;
    String postCode;
    String city;


    public Address() {
    }

    public Address(String street, String postCode, String city){
        this.street = street;
        this.postCode = postCode;
        this.city = city;
    }


    public Address(String careOf, String street, String postCode, String city) {
        super();
        this.street = street;
        this.postCode = postCode;
        this.city = city;
    }


}
